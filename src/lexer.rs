const EXTRA_IDENT_CHARACTERS: [char; 12] =
    ['_', '-', '+', ':', '<', '>', '[', ']', '(', ')', '{', '}'];

#[derive(Debug, PartialEq, Eq)]
pub enum TaskToken {
    // git diff
    AddedMarker,
    RemovedMarker,
    // task
    Text(String),
    DateTime(String),
    TaskState(TaskState),
    TaskTitle(String),
    ItemStartMarker,
    Deadline,
    Scheduled,
    Closed,
    Newline,
    Empty,
    Eof,
}

// TODO: eq should be a custom
#[derive(Debug, PartialEq, Eq)]
pub struct TaskState {
    pub keyword: String,
    pub symbol: String,
}

pub struct Lexer {
    input: Vec<char>,
    position: usize,
    read_position: usize,
    ch: Option<char>,
    org_cfg: OrgConfig,
}

pub struct OrgConfig {
    states: Vec<TaskState>,
}

impl Lexer {
    pub fn new(input: Vec<char>, org_cfg: OrgConfig) -> Self {
        Self {
            input,
            position: 0,
            read_position: 0,
            ch: None,
            org_cfg,
        }
    }
    pub fn next_token(&mut self, prev_token: Option<&TaskToken>) -> TaskToken {
        self.next_char();

        if self.ch.is_none() {
            return TaskToken::Eof;
        }

        match prev_token {
            Some(TaskToken::Deadline | TaskToken::Scheduled | TaskToken::Closed) => {
                return TaskToken::DateTime(self.read_to_newline().iter().collect());
            }
            Some(TaskToken::TaskState { .. } | TaskToken::ItemStartMarker) => {
                let title = self.read_to_newline();
                return if title.is_empty() {
                    TaskToken::Empty
                } else {
                    TaskToken::TaskTitle(title.iter().collect())
                };
            }
            _ => {}
        }

        let tok = match self.ch {
            None => TaskToken::Eof,
            Some('*') => TaskToken::ItemStartMarker,
            Some('+') => TaskToken::AddedMarker,
            Some('-') => TaskToken::RemovedMarker,
            Some('\n') => TaskToken::Newline,
            Some(_) => {
                let ident = self.read_ident();
                match get_keyword_token(&ident, &self.org_cfg.states) {
                    Some(tok) => tok,
                    None => {
                        let text = ident.iter().collect::<String>();
                        if text.is_empty() {
                            TaskToken::Empty
                        } else {
                            TaskToken::Text(text.to_owned())
                        }
                    }
                }
            }
        };

        tok
    }

    fn next_char(&mut self) {
        let char = if self.read_position >= self.input.len() {
            None
        } else {
            Some(self.input[self.read_position])
        };

        self.position = self.read_position;
        self.read_position += 1;

        self.ch = char
    }

    fn read_ident(&mut self) -> Vec<char> {
        let position = self.position;
        while self.position < self.input.len() && self.ch.map(is_letter).unwrap_or(false) {
            self.next_char();
        }

        self.input[position..self.position].to_vec()
    }

    fn read_to_newline(&mut self) -> Vec<char> {
        let position = self.position;
        while self.position < self.input.len() && self.ch != Some('\n') {
            self.next_char();

            let ident = self.read_ident();
            if get_keyword_token(&ident, &self.org_cfg.states).is_some() {
                self.position -= ident.len() + 1;
                self.read_position -= ident.len() + 1;
                break;
            }
        }

        if self.ch == Some('\n') {
            self.read_position -= 1;
        }

        self.input[position..self.position].to_vec()
    }
}

impl OrgConfig {
    pub fn new(states: Vec<TaskState>) -> Self {
        Self { states }
    }
}

fn get_keyword_token(ident: &[char], states: &[TaskState]) -> Option<TaskToken> {
    let max_keyword_len = 128;
    let ident: String = ident.iter().take(max_keyword_len).collect();

    if let Some(state) = states
        .iter()
        .find(|state| ident.starts_with(&state.keyword))
    {
        return Some(TaskToken::TaskState(TaskState {
            symbol: state.symbol.clone(),
            keyword: state.keyword.clone(),
        }));
    }

    match &ident[..] {
        "DEADLINE:" => Some(TaskToken::Deadline),
        "SCHEDULED:" => Some(TaskToken::Scheduled),
        "CLOSED:" => Some(TaskToken::Closed),
        _ => None,
    }
}

fn is_letter(ch: char) -> bool {
    ch.is_ascii_lowercase() || ch.is_ascii_uppercase() || EXTRA_IDENT_CHARACTERS.contains(&ch)
}
