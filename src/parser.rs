use std::{collections::VecDeque, fmt::Display};

use crate::lexer::{TaskState, TaskToken};

#[derive(Debug, PartialEq, Eq)]
pub enum TaskDiff {
    AddedTaskWithState {
        symbol: String,
        task_title: String,
    },
    ChangedDeadline {
        task_title: String,
        old_deadline: String,
        new_deadline: String,
    },
    ChangedScheduled {
        task_title: String,
        old_scheduled: String,
        new_scheduled: String,
    },
}

impl TaskDiff {
    pub fn from_tokens(mut tokens: VecDeque<TaskToken>) -> Result<Vec<Self>, String> {
        type TT = TaskToken;

        let mut diffs = vec![];

        while let Some(tok) = tokens.pop_front() {
            match tok {
                TT::AddedMarker => {
                    while let Some(tok) = tokens.pop_front() {
                        match tok {
                            TT::ItemStartMarker => {
                                if let Some(TT::TaskState { .. }) = tokens.front() {
                                } else {
                                    return Err("task item should have a state".to_owned());
                                }
                            }
                            TT::TaskState(TaskState { symbol, keyword }) => {
                                if let Some(TT::TaskTitle(title)) = tokens.pop_front() {
                                    let diff = TaskDiff::AddedTaskWithState {
                                        symbol,
                                        task_title: title,
                                    };
                                    diffs.push(diff);
                                } else {
                                    return Err(format!(
                                        "task state '{keyword}' should be followed by task title"
                                    ));
                                }

                                break;
                            }
                            TT::Newline => {
                                break;
                            }
                            _ => {}
                        }
                    }
                }
                // ignore removed lines
                TT::RemovedMarker => {
                    while let Some(tok) = tokens.pop_front() {
                        if let TT::Newline = tok {
                            break;
                        }
                    }
                }
                TT::TaskTitle(title) => {
                    let mut old_date = None;
                    let mut new_date = None;
                    while let Some(tok) = tokens.pop_front() {
                        match tok {
                            TT::AddedMarker => match tokens.pop_front() {
                                Some(TT::Deadline) => {
                                    if let Some(TT::DateTime(date_time)) = tokens.pop_front() {
                                        if let Some(old_date) = old_date {
                                            let diff = Self::ChangedDeadline {
                                                task_title: title,
                                                old_deadline: old_date,
                                                new_deadline: date_time,
                                            };
                                            diffs.push(diff);

                                            break;
                                        }

                                        new_date = Some(date_time);
                                    } else {
                                        return Err("deadline should be followed by a date time"
                                            .to_string());
                                    }
                                }
                                Some(TT::Scheduled) => {
                                    if let Some(TT::DateTime(date_time)) = tokens.pop_front() {
                                        if let Some(old_date) = old_date {
                                            let diff = Self::ChangedScheduled {
                                                task_title: title,
                                                old_scheduled: old_date,
                                                new_scheduled: date_time,
                                            };
                                            diffs.push(diff);

                                            break;
                                        }
                                        new_date = Some(date_time);
                                    } else {
                                        return Err("schedule should be followed by a date time"
                                            .to_string());
                                    }
                                }
                                Some(TT::Newline) => {
                                    break;
                                }
                                _ => {}
                            },
                            TT::RemovedMarker => match tokens.pop_front() {
                                Some(TT::Deadline) => {
                                    if let Some(TT::DateTime(date_time)) = tokens.pop_front() {
                                        if let Some(new_date) = new_date {
                                            let diff = Self::ChangedDeadline {
                                                task_title: title,
                                                old_deadline: date_time,
                                                new_deadline: new_date,
                                            };
                                            diffs.push(diff);

                                            break;
                                        }
                                        old_date = Some(date_time);
                                    } else {
                                        return Err("deadline should be followed by a date time"
                                            .to_string());
                                    }
                                }
                                Some(TT::Scheduled) => {
                                    if let Some(TT::DateTime(date_time)) = tokens.pop_front() {
                                        if let Some(new_date) = new_date {
                                            let diff = Self::ChangedScheduled {
                                                task_title: title,
                                                old_scheduled: date_time,
                                                new_scheduled: new_date,
                                            };
                                            diffs.push(diff);

                                            break;
                                        }
                                        old_date = Some(date_time);
                                    } else {
                                        return Err("schedule should be followed by a date time"
                                            .to_string());
                                    }
                                }
                                Some(TT::Newline) => {
                                    break;
                                }
                                _ => {}
                            },
                            TT::Newline => {}
                            _ => break,
                        }
                    }
                }
                _ => {}
            }
        }

        Ok(diffs)
    }

    pub fn pretty_string(&self, note_category: impl Display) -> String {
        match self {
            Self::AddedTaskWithState { symbol, task_title } => format!(
                "<{note_category}{symbol}> {title}",
                title = task_title.trim()
            ),
            Self::ChangedDeadline {
                task_title,
                old_deadline,
                new_deadline,
            } => format!(
                "<{note_category}{icon}> [{title}] {old} -> {new}",
                icon = '⧗',
                title = task_title.trim(),
                old = old_deadline.trim(),
                new = new_deadline.trim()
            ),
            Self::ChangedScheduled {
                task_title,
                old_scheduled,
                new_scheduled,
            } => format!(
                "<{note_category}{icon}> [{title}] {old} -> {new}",
                icon = '⧖',
                title = task_title.trim(),
                old = old_scheduled.trim(),
                new = new_scheduled.trim()
            ),
        }
    }
}
