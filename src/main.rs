use std::{
    env,
    process::{exit, Command},
};

use itertools::Itertools;
use lexer::{Lexer, OrgConfig, TaskState, TaskToken};
use parser::TaskDiff;

mod lexer;
mod parser;

fn process(input: &str, org_cfg: OrgConfig) -> Result<Vec<TaskDiff>, String> {
    let mut lexer = Lexer::new(input.chars().collect(), org_cfg);

    let mut tokens = vec![lexer.next_token(None)];

    loop {
        let token = lexer.next_token(tokens.last());

        if let TaskToken::Eof = token {
            break;
        }

        tokens.push(token);
    }

    let tokens: Vec<_> = tokens
        .into_iter()
        .filter(|t| *t != TaskToken::Empty)
        .collect();

    TaskDiff::from_tokens(tokens.into())
}

fn join_task_states(extra_states: Vec<TaskState>) -> Vec<TaskState> {
    let default_states = vec![
        TaskState {
            keyword: "TODO".to_owned(),
            symbol: "+".to_owned(),
        },
        TaskState {
            keyword: "DONE".to_owned(),
            symbol: "✓".to_owned(),
        },
        TaskState {
            keyword: "CANCELED".to_owned(),
            symbol: "×".to_owned(),
        },
    ];

    default_states
        .into_iter()
        .chain(extra_states)
        .unique_by(|state| state.keyword.clone())
        .collect_vec()
}

/// ```plain
/// parse states in the form ["x, DONE", "-, WIP"]
///                            ^    ^
///                            |    |
///                           /      \
///                          /        \
///                       symbol    keyword
///```
fn parse_task_states(states: &[String]) -> Vec<TaskState> {
    states
        .iter()
        .filter_map(|str| {
            let (symbol, keyword) = str.split_once(',')?;
            Some(TaskState {
                symbol: symbol.to_owned(),
                keyword: keyword.to_owned(),
            })
        })
        .collect()
}

fn main() {
    let note_category = env::args().nth(1).expect("note category is required");
    let task_states = join_task_states(parse_task_states(
        env::args().skip(2).collect::<Vec<_>>().as_slice(),
    ));

    let diff_file = format!("todos/{note_category}.org");
    let line_nums_to_show = 100_000.to_string();
    let out = Command::new("git")
        .args([
            "diff",
            "--staged",
            &format!("-U{line_nums_to_show}"),
            &diff_file,
        ])
        .output()
        .expect("git diff shouldn't fail")
        .stdout;

    let diffs = process(
        &String::from_utf8(out).expect("git diff output should be valid utf-8"),
        OrgConfig::new(task_states),
    );
    match diffs {
        Ok(diffs) => {
            let commit_msg = diffs
                .into_iter()
                .map(|d| d.pretty_string(&note_category))
                .collect::<Vec<_>>()
                .join("\n");
            println!("{commit_msg}");
        }
        Err(err) => {
            eprintln!("{err}");
            exit(1);
        }
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_process() {
        let input_output = vec![
            (
                r#"
 * TODO ignored 1
   DEADLINE: <2023-11-19 Sun>

+* DONE done task
   DEADLINE: <2023-11-18 Sat>

+* TODO todo task
+  DEADLINE: <2023-11-20 Mon>
+
 * TODO scheduled changed
+   SCHEDULED: <2023-11-25 Sat>
-   SCHEDULED: <2023-11-24 Fri>

 * TODO deadline changed
-  DEADLINE: <2023-11-17 Fri>
+  DEADLINE: <2023-11-18 Sat>

 * TODO ignored 2
   DEADLINE: <2023-11-25 Sat>

+ this is new

- * TODO removed task
-   DEADLINE: <2023-11-25 Sat>
        "#,
                Ok(vec![
                    TaskDiff::AddedTaskWithState {
                        symbol: " ".to_owned(),
                        task_title: "done task".to_owned(),
                    },
                    TaskDiff::AddedTaskWithState {
                        symbol: " ".to_owned(),
                        task_title: "todo task".to_owned(),
                    },
                    TaskDiff::ChangedScheduled {
                        task_title: "scheduled changed".to_owned(),
                        old_scheduled: "<2023-11-24 Fri>".to_owned(),
                        new_scheduled: "<2023-11-25 Sat>".to_owned(),
                    },
                    TaskDiff::ChangedDeadline {
                        task_title: "deadline changed".to_owned(),
                        old_deadline: "<2023-11-17 Fri>".to_owned(),
                        new_deadline: "<2023-11-18 Sat>".to_owned(),
                    },
                ]),
            ),
            (
                r#"
diff --git a/todos/school.org b/todos/school.org
index 1f04a4a..05f58bc 100644
--- a/todos/school.org
+++ b/todos/school.org
@@ -1,14 +1,8 @@
-* TODO NSCS: cheatsheet
-  DEADLINE: <2023-11-23 Thu>
-
 * TODO NSCS: prep for test
   DEADLINE: <2023-11-19 Sun>

-* TODO NSCS: prep for test
-  DEADLINE: <2023-11-18 Sat>
-
 * TODO DE: prep for test
-  DEADLINE: <2023-11-20 Mon>
+  DEADLINE: <2023-11-18 Sat>

 * TODO DBI homework
   DEADLINE: <2023-11-25 Sat>
"#,
                Ok(vec![TaskDiff::ChangedDeadline {
                    task_title: "DE: prep for test".to_owned(),
                    old_deadline: "<2023-11-20 Mon>".to_owned(),
                    new_deadline: "<2023-11-18 Sat>".to_owned(),
                }]),
            ),
            (
                r#"
diff --git a/todos/personal.org b/todos/personal.org
index 417fdc3..f0835c4 100644
--- a/todos/personal.org
+++ b/todos/personal.org
@@ -1,17 +1,11 @@
-* TODO go shopping
-  DEADLINE: <2023-11-18 Sat>
-
-* DONE write lexer+parser for notes
-  CLOSED: [2023-11-18 Sat 15:27] DEADLINE: <2023-11-18 Sat>
-
 * DONE chores
   CLOSED: [2023-11-16 Thu 17:55] DEADLINE: <2023-11-16 Thu>

 - take out the trash
 - dishes

-* DONE disable lucygschwantner.com domain
-  CLOSED: [2023-11-18 Sat 15:35] DEADLINE: <2023-11-18 Sat>
+* TODO disable lucygschwantner.com domain
+  DEADLINE: <2023-11-18 Sat>

 * CANCELED buy ikea furniture
   CLOSED: [2023-11-16 Thu 18:09] DEADLINE: <2023-11-17 Fri>
             "#,
                Ok(vec![TaskDiff::AddedTaskWithState {
                    symbol: " ".to_owned(),
                    task_title: "disable lucygschwantner.com domain".to_owned(),
                }]),
            ),
            (
                r#"
 * CANCELED buy ikea furniture
-  CLOSED: [2023-11-16 Thu 18:09] DEADLINE: <2023-11-17 Fri>
+  DEADLINE: <2023-11-17 Fri> CLOSED: [2023-11-16 Thu 18:09]

-* DONE deal with A1 bill
-  CLOSED: [2023-11-17 Fri 17:18] DEADLINE: <2023-11-17 Fri>
+* TODO deal with A1 bill
+  DEADLINE: <2023-11-17 Fri>

 * DONE add jem ticket for CI rust caching
   CLOSED: [2023-11-15 Wed 18:50] DEADLINE: <2023-11-15 Wed>

+* CANCELED get clothes and super glue in leibnitz
+  DEADLINE: <2023-11-17 Fri>

 * DONE go shopping
   CLOSED: [2023-11-13 Mon 18:34] DEADLINE: <2023-11-13 Mon>
             "#,
                Ok(vec![
                    TaskDiff::AddedTaskWithState {
                        symbol: " ".to_owned(),
                        task_title: "deal with A1 bill".to_owned(),
                    },
                    TaskDiff::AddedTaskWithState {
                        symbol: " ".to_owned(),
                        task_title: "get clothes and super glue in leibnitz".to_owned(),
                    },
                ]),
            ),
            (
                r#"
+* task name missing state
   DEADLINE: <2023-11-01 Fri>
             "#,
                Err("task item should have a state".to_owned()),
            ),
            (
                r#"
diff --git a/todos/school.org b/todos/school.org
index 1f04a4a..0552b88 100644
--- a/todos/school.org
+++ b/todos/school.org
@@ -4,8 +4,8 @@
 * TODO NSCS: prep for test
   DEADLINE: <2023-11-19 Sun>

-* TODO NSCS: prep for test
-  DEADLINE: <2023-11-18 Sat>
+* DONE NSCS: prep for test
+  DEADLINE: <2023-11-18 Sat> CLOSED: [2023-11-18 Sat 22:54]

 * TODO DE: prep for test
   DEADLINE: <2023-11-20 Mon>
             "#,
                Ok(vec![TaskDiff::AddedTaskWithState {
                    symbol: " ".to_owned(),
                    task_title: "NSCS: prep for test".to_owned(),
                }]),
            ),
        ];

        for (input, output) in input_output {
            let task_states = vec![
                TaskState {
                    keyword: "TODO".to_owned(),
                    symbol: " ".to_owned(),
                },
                TaskState {
                    keyword: "DONE".to_owned(),
                    symbol: " ".to_owned(),
                },
                TaskState {
                    keyword: "CANCELED".to_owned(),
                    symbol: " ".to_owned(),
                },
            ];
            let diffs = process(input, OrgConfig::new(task_states));
            assert_eq!(diffs, output);
        }
    }
}
